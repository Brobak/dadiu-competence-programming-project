﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
	[TestClass()]
	public class EventBusTests
	{
		[TestMethod()]
		public void AddListenerAndTriggerTest()
		{
			//Arrange
			string stringToSet = "";

			//Act
			EventBus.AddListener(EventBus.EventType.PickupHint, () => stringToSet = "Success");

			//Assert
			Assert.AreEqual(stringToSet, "");

			//Act
			EventBus.TriggerEvent(EventBus.EventType.PickupHint);

			//Assert
			Assert.AreEqual(stringToSet, "Success");
		}

		[TestMethod()]
		public void RemoveListenerTest()
		{
			//Arrange
			string stringToSet = "Success";
			Action method = () => stringToSet = "Failure";
			EventBus.AddListener(EventBus.EventType.PickupHint, method);

			//Act
			EventBus.RemoveListener(EventBus.EventType.PickupHint, method);
			EventBus.TriggerEvent(EventBus.EventType.PickupHint);

			//Assert
			Assert.AreEqual(stringToSet, "Success");
		}

		[TestMethod()]
		public void TriggerEmptyTest()
		{
			//Act
			EventBus.TriggerEvent(EventBus.EventType.PickupHint);
		}

		[TestMethod()]
		public void RemoveListenerEmptyTest()
		{
			//Arrange
			string stringToSet = "Success";
			Action method = () => stringToSet = "Failure";

			//Act
			EventBus.RemoveListener(EventBus.EventType.PickupHint, method);
		}

		[TestMethod()]
		public void MultipleListenerTest()
		{
			//Arrange
			int invocationCounter = 0;
			Action method = () => invocationCounter++;
			EventBus.RemoveListener(EventBus.EventType.PickupHint, method);
			EventBus.AddListener(EventBus.EventType.PickupHint, method);
			EventBus.RemoveListener(EventBus.EventType.PickupHint, method);
			EventBus.AddListener(EventBus.EventType.PickupHint, method);
			EventBus.AddListener(EventBus.EventType.PickupHint, method);
			EventBus.AddListener(EventBus.EventType.PickupHint, method);
			EventBus.AddListener(EventBus.EventType.PickupHint, method);
			EventBus.AddListener(EventBus.EventType.PickupHint, method);
			EventBus.RemoveListener(EventBus.EventType.PickupHint, method);
			EventBus.RemoveListener(EventBus.EventType.PickupHint, method);

			//Act
			EventBus.TriggerEvent(EventBus.EventType.PickupHint);

			//Assert
			Assert.AreEqual(invocationCounter, 3);
		}
	}
}