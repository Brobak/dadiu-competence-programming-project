﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
	[TestClass()]
	public class NumberExtensionsTests
	{
		[TestMethod()]
		public void FloatModTest()
		{
			//Arrange
			float positiveResult, negativeResult, fractionResult, zeroResult;

			//Act
			positiveResult = 102f.Mod(5);
			negativeResult = (-102f).Mod(5);
			fractionResult = 101.5f.Mod(5);
			zeroResult = 0f.Mod(5);

			//Assert
			Assert.AreEqual(2, positiveResult);
			Assert.AreEqual(3, negativeResult);
			Assert.AreEqual(1.5f, fractionResult, 1e-5f);
			Assert.AreEqual(0, zeroResult);
		}

		[TestMethod()]
		public void IntModTest()
		{
			//Arrange
			int positiveResult, negativeResult, zeroResult;

			//Act
			positiveResult = 102.Mod(5);
			negativeResult = (-102).Mod(5);
			zeroResult = 0.Mod(5);

			//Assert
			Assert.AreEqual(2, positiveResult);
			Assert.AreEqual(3, negativeResult);
			Assert.AreEqual(0, zeroResult);
		}
	}
}