﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// This is an example of a test class
/// To create your own tests make a class that inherits from BaseUnityTest and has an empty constructor, and create your test methods in it.
/// A test method must have return type bool and the name must end with Test
/// </summary>
public class DemoTest : BaseUnityTest
{
	//This is an example of a test that will succeed
    private bool TrueMathTest() 
    {
    	int result = 2 + 2;

    	return 4 == result;
    }

    //This is an example of a test that will fail. You can write messages to Debug log as usual
    private bool FalseMathTest() 
    {
    	int result = 2 + 3;

    	if(4 == result) 
    	{
    		return true;
    	}
    	else
    	{
    		Debug.LogError("I'm bad at math");
    		return false;
    	}
    }
}
