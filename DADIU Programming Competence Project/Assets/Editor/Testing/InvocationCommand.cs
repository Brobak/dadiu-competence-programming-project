﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class InvocationCommand
{
    public string name { get; private set; }
    private Func<bool> method { get; set; }

    public InvocationCommand(string name, Func<bool> method)
    {
        this.name = name;
        this.method = method;
    }

    public bool Invoke()
    {
        return method();
    }
}
