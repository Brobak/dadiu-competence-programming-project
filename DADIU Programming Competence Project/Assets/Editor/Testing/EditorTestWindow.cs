﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class EditorTestWindow : EditorWindow
{
	private List<BaseUnityTest> _temporaryTestClasses;
	private Dictionary<InvocationCommand, RunState> runStates;
	private Dictionary<BaseUnityTest, bool> foldedOut;

	private List<BaseUnityTest> temporaryTestClasses
	{
		get
		{
			if (_temporaryTestClasses == null)
			{
				_temporaryTestClasses = BaseUnityTest.testClasses.ToList();
				runStates = _temporaryTestClasses
					.SelectMany(tc => tc.testCases)
					.ToDictionary(test => test, test => RunState.NotRun);
				foldedOut = _temporaryTestClasses.ToDictionary(key => key, key => false);
			}

			return _temporaryTestClasses;
		}
	}

	[MenuItem("Window/Custom Testing")]
	public static void ShowWindow()
	{
		GetWindow<EditorTestWindow>();
	}

	private void OnGUI()
	{
		if (GUILayout.Button("Refresh"))
		{
			_temporaryTestClasses = null;
		}

		GUILayout.Space(5);
		if (GUILayout.Button("Run All Tests"))
		{
			List<InvocationCommand> allTestCases = temporaryTestClasses
				.SelectMany(tc => tc.testCases).ToList();
			foreach (InvocationCommand test in allTestCases)
			{
				RunTest(test);
			}
		}

		GUILayout.Label("Individual Tests");
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Collapse All"))
		{
			foreach(BaseUnityTest testClass in temporaryTestClasses)
			{
				foldedOut[testClass] = false;
			}
		}
		if(GUILayout.Button("Expand All"))
		{
			foreach (BaseUnityTest testClass in temporaryTestClasses)
			{
				foldedOut[testClass] = true;
			}
		}
		GUILayout.EndHorizontal();

		foreach (BaseUnityTest testClass in temporaryTestClasses)
		{
			foldedOut[testClass] = EditorGUILayout.Foldout(foldedOut[testClass],
				testClass.GetType().Name, GetFoldoutStyle(GetClassSuccesColor(testClass)));
			if (foldedOut[testClass])
			{
				foreach (InvocationCommand test in testClass.testCases)
				{
					if (GUILayout.Button("Run " + test.name))
					{
						RunTest(test);
					}
					ShowResult(runStates[test]);
				}
			}
		}
	}

	private GUIStyle GetFoldoutStyle(Color textColor)
	{
		GUIStyle result = new GUIStyle(EditorStyles.foldout);
		result.normal.textColor = textColor;
		result.onNormal.textColor = textColor;
		result.hover.textColor = textColor;
		result.onHover.textColor = textColor;
		result.focused.textColor = textColor;
		result.onFocused.textColor = textColor;
		result.active.textColor = textColor;
		result.onActive.textColor = textColor;

		return result;
	}

	private Color GetClassSuccesColor(BaseUnityTest testClass)
	{
		var tempRuns = testClass.testCases.Select(tc => runStates[tc]);
		if (tempRuns.Any(run => run == RunState.Failed))
		{
			return Color.red;
		}
		if (tempRuns.All(run => run == RunState.Succeeded))
		{
			return Color.green;
		}

		return GUI.contentColor;
	}

	private void ShowResult(RunState runState)
	{
		string message = "";
		Color color = GUI.contentColor;
		switch (runState)
		{
			case RunState.Failed:
				message = "Failed!";
				color = Color.red;
				break;
			case RunState.NotRun:
				message = "Not Run";
				break;
			case RunState.Succeeded:
				message = "Succeeded!";
				color = Color.green;
				break;
		}
		GUILayout.BeginHorizontal();
		GUIStyle style = new GUIStyle(GUI.skin.label);
		style.normal.textColor = color;
		GUILayout.Label(message, style);
		GUILayout.EndHorizontal();
	}

	private void RunTest(InvocationCommand testCase)
	{
		bool success;

		try
		{
			success = testCase.Invoke();
		}
		catch (Exception e)
		{
			success = false;
			Debug.LogError(testCase.name + " failed with error: " + e);
		}

		runStates[testCase] = success ? RunState.Succeeded : RunState.Failed;
	}

	private enum RunState
	{
		NotRun,
		Failed,
		Succeeded
	}
}
