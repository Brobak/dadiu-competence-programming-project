﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Handles testing the extensions for collections defined in <see cref="CollectionExtensions"/>
/// </summary>
public class CollectionExtensionTests : BaseUnityTest
{
	private bool ArrayGetRandomElementTest()
	{
		//Arrange
		int[] array = Enumerable.Range(0, 25).ToArray();

		for(int i = 0; i < 100; i++)
		{
			//Act
			int randomNr = array.GetRandomElement();

			//Assert
			if(!array.Contains(randomNr))
			{
				return false;
			}
		}

		return true;
	}

	private bool ListGetRandomElementTest()
	{
		//Arrange
		List<int> list = Enumerable.Range(0, 25).ToList();

		for (int i = 0; i < 100; i++)
		{
			//Act
			int randomNr = list.GetRandomElement();

			//Assert
			if (!list.Contains(randomNr))
			{
				return false;
			}
		}

		return true;
	}

	private bool CollectionGetRandomElementTest()
	{
		//Arrange
		IEnumerable<int> collection = Enumerable.Range(0, 25);

		for (int i = 0; i < 100; i++)
		{
			//Act
			int randomNr = collection.GetRandomElement();

			//Assert
			if (!collection.Contains(randomNr))
			{
				return false;
			}
		}

		return true;
	}

	private bool ArrayGetRandomElementsTest()
	{
		//Arrange
		int[] array = Enumerable.Range(0, 25).ToArray();

		for (int i = 0; i < 100; i++)
		{
			//Act
			int[] randomNrs = array.GetRandomElements(10).ToArray();

			//Assert
			foreach (int randomNr in randomNrs)
			{
				if (!array.Contains(randomNr))
				{
					return false;
				} 
			}
			if(randomNrs.Distinct().Count() != randomNrs.Length)
			{
				return false;
			}
		}

		return true;
	}

	private bool ListGetRandomElementsTest()
	{
		//Arrange
		List<int> list = Enumerable.Range(0, 25).ToList();

		for (int i = 0; i < 100; i++)
		{
			//Act
			List<int> randomNrs = list.GetRandomElements(10).ToList();

			//Assert
			foreach (int randomNr in randomNrs)
			{
				if (!list.Contains(randomNr))
				{
					return false;
				}
			}
			if (randomNrs.Distinct().Count() != randomNrs.Count)
			{
				return false;
			}
		}

		return true;
	}

	private bool CollectionGetRandomElementsTest()
	{
		//Arrange
		IEnumerable<int> collection = Enumerable.Range(0, 25);

		for (int i = 0; i < 100; i++)
		{
			//Act
			int[] randomNrs = collection.GetRandomElements(10).ToArray();

			//Assert
			foreach (int randomNr in randomNrs)
			{
				if (!collection.Contains(randomNr))
				{
					return false;
				}
			}
			if (randomNrs.Distinct().Count() != randomNrs.Length)
			{
				return false;
			}
		}

		return true;
	}
}
