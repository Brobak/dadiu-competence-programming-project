﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public abstract class BaseUnityTest
{
	public static IEnumerable<BaseUnityTest> testClasses
	{
		get
		{
			foreach (Type type in typeof(BaseUnityTest).Assembly.GetTypes()
				.Where(t => t.IsSubclassOf(typeof(BaseUnityTest))))
			{
				yield return (BaseUnityTest)type.GetConstructor(new Type[0]).Invoke(new object[0]);
			}
		}
	}

	protected List<InvocationCommand> _testCases;

	public virtual IEnumerable<InvocationCommand> testCases
	{
		get
		{
			if (_testCases == null)
			{
				_testCases = new List<InvocationCommand>();
				foreach (MethodInfo method in GetType().GetMethods(BindingFlags.NonPublic |
																   BindingFlags.Instance |
																   BindingFlags.DeclaredOnly)
					   .Where(method => method.Name.EndsWith("Test") &&
										method.ReturnType == typeof(bool) &&
										!method.IsStatic))
				{
					MethodInfo method1 = method;
					Func<bool> tempMethod = () => (bool)method1.Invoke(this, new object[0]);
					InvocationCommand invocator = new InvocationCommand(method1.Name, tempMethod);

					_testCases.Add(invocator);
				}
			}
			foreach (InvocationCommand testCase in _testCases)
			{
				yield return testCase;
			}
		}
	}
}
