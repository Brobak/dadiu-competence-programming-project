﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The goal which will cause the player to win if found
/// </summary>
public class Goal : MonoBehaviour
{
	[SerializeField, Tooltip("The position to go to when winning")]
	private Vector3 winPosition = new Vector3(-28, 17, -107);
	[SerializeField, Tooltip("The rotation to set when winning")]
	private Vector3 winRotation = new Vector3(5, 10, 0);
	
	private CameraMovement player;
	private PlayerMovement playerMover;
	private Rigidbody playeRigidbody;

	private void Awake()
	{
		player = FindObjectOfType<CameraMovement>();
		playerMover = player.GetComponent<PlayerMovement>();
		playeRigidbody = player.GetComponent<Rigidbody>();
	}
	
	private void OnTriggerEnter(Collider trigger)
	{
		EventBus.TriggerEvent(EventBus.EventType.WinGame);
		SetPlayerWin();
	}

	private void SetPlayerWin()
	{
		player.transform.position = winPosition;
		player.SetRotationEuler(winRotation);
		player.enabled = false;
		playerMover.enabled = false;
		playeRigidbody.constraints = RigidbodyConstraints.FreezeAll;
	}
}
