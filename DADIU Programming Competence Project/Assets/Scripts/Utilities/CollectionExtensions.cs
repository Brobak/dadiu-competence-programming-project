﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

/// <summary>
/// Holds extensions for some collections for when LINQ just isn't enough
/// </summary>
public static class CollectionExtensions
{
	/// <summary>
	/// Returns a random value from the given array.
	/// Complexity is O(1)
	/// </summary>
	public static T GetRandomElement<T>(this T[] array)
	{
		return array[Random.Range(0, array.Length)];
	}

	/// <summary>
	/// Returns a random value from the given list.
	/// Complexity is O(1)
	/// </summary>
	public static T GetRandomElement<T>(this IList<T> list)
	{
		return list[Random.Range(0, list.Count)];
	}

	/// <summary>
	/// Returns a random value from the given collection.
	/// Complexity is O(n)
	/// </summary>
	public static T GetRandomElement<T>(this IEnumerable<T> collection)
	{
		T current = default(T);
		int count = 1;

		foreach (T element in collection)
		{
			if (Random.Range(0, count) == 0)
			{
				current = element;
			}
			count++;
		}
		if (count == 0)
		{
			throw new IndexOutOfRangeException("Collection was empty");
		}

		return current;
	}

	/// <summary>
	/// Returns the specified number of random values from the given collection.
	/// Complexity is O(k) where k is amount
	/// </summary>
	public static IEnumerable<T> GetRandomElements<T>(this IList<T> list, int amount)
	{
		if(amount > list.Count)
		{
			throw new ArgumentOutOfRangeException("Not anough elements in collection");
		}
		
		int[] indexes = Enumerable.Range(0, list.Count).ToArray();

		for(int i = 0; i < amount; i++)
		{
			int index = Random.Range(i, list.Count);

			int swapTemp = indexes[index];
			indexes[index] = indexes[i];
			indexes[i] = swapTemp;

			yield return list[indexes[i]];
		}
	}

	/// <summary>
	/// Returns the specified number of random values from the given collection.
	/// Complexity is O(n)
	/// </summary>
	public static IEnumerable<T> GetRandomElements<T>(this IEnumerable<T> collection, int amount)
	{
		T[] current = new T[amount];
		int count = 0;

		foreach (T element in collection)
		{
			if (count < amount)
			{
				current[count] = element;
			}
			else if (Random.Range(0, count + 1) / amount < 1)
			{
				current[Random.Range(0, amount)] = element;
			}
			count++;
		}
		if (count < amount)
		{
			throw new IndexOutOfRangeException("Not enough elements in collection");
		}

		return current;
	}
}
