﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Holds generally usefull constants
/// </summary>
public static class UtilityConstants
{
	public static readonly Vector3[] orthogonalDirections = 
		{ Vector3.up, Vector3.right, Vector3.down, Vector3.left, Vector3.forward, Vector3.back };
}
