﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Holds some mathematic extensions for the numeric types
/// </summary>
public static class NumberExtensions
{
	/// <summary>
	/// True modulo function
	/// </summary>
	public static float Mod(this float first, float second)
	{
		first %= second;
		return (first < 0) ? first + second : first;
	}

	/// <summary>
	/// True modulo function
	/// </summary>
	public static int Mod(this int first, int second)
	{
		first %= second;
		return (first < 0) ? first + second : first;
	}
}
