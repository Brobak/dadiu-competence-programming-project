﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Holds extensions for transform to allow setting only a single value in position, rotation and so on
/// </summary>
public static class TransformExtensions
{
	public static void SetPositionX(this Transform transform, float newX)
	{
		transform.position = new Vector3(newX, transform.position.y, transform.position.z);
	}

	public static void SetPositionY(this Transform transform, float newY)
	{
		transform.position = new Vector3(transform.position.x, newY, transform.position.z);
	}

	public static void SetPositionZ(this Transform transform, float newZ)
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, newZ);
	}

	public static void SetLocalPositionX(this Transform transform, float newX)
	{
		transform.localPosition = 
			new Vector3(newX, transform.localPosition.y, transform.localPosition.z);
	}

	public static void SetLocalPositionY(this Transform transform, float newY)
	{
		transform.localPosition = 
			new Vector3(transform.localPosition.x, newY, transform.localPosition.z);
	}

	public static void SetLocalPositionZ(this Transform transform, float newZ)
	{
		transform.localPosition = 
			new Vector3(transform.localPosition.x, transform.localPosition.y, newZ);
	}
}
