﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalPointer : MonoBehaviour
{
    private Goal goal;

    private void Awake()
    {
        goal = FindObjectOfType<Goal>();
    }

    private void Update()
    {
        transform.forward = goal.transform.position - transform.position;
    }
}
