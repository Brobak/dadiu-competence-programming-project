﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles winning and what happens after winning
/// </summary>
public class WinContext : MonoBehaviour
{
    [SerializeField, NotNull, Tooltip("The parent of all the buttons that should be shown on the end screen")]
    private RectTransform endScreen;
    [SerializeField, NotNull(IgnorePrefab = true), Tooltip("The arrow pointing towards the goal, which should be deactivated when winning")]
    private GoalPointer goalPointer;
    [SerializeField, NotNull(IgnorePrefab = true), Tooltip("The pause menu UI")]
    private PauseMenu pauseMenu;

    private Text message;

    private void Awake()
    {
        message = GetComponent<Text>();
		EventBus.AddListener(EventBus.EventType.WinGame, Win);
    }

	private void OnDestroy()
	{
		EventBus.RemoveListener(EventBus.EventType.WinGame, Win);
	}

	private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    /// <summary>
    /// Handles what should happen when winning
    /// </summary>
    public void Win()
    {
        gameObject.SetActive(true);
        enabled = true;
        message.enabled = true;
        endScreen.gameObject.SetActive(true);
        goalPointer.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
