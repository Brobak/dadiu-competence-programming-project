﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary> 
/// Meant to be called by a button click. Will load the level with the name specified in scene to load 
/// </summary> 
public class LoadLevelButton : MonoBehaviour
{
	[SerializeField, Tooltip("The name of the level to load")]
	private string sceneToLoad;

	/// <summary> 
	/// Loads the level specified by the field <see cref="sceneToLoad"/> 
	/// </summary> 
	public void LoadLevel()
	{
		SceneManager.LoadScene(sceneToLoad);
	}
}