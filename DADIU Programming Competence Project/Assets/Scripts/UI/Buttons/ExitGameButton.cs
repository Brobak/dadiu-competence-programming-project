﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Can quit the game when the player clicks a button
/// </summary>
public class ExitGameButton : MonoBehaviour
{
	/// <summary>
	/// Is called when the exit button is clicked
	/// </summary>
	public void ExitGame()
	{
		Application.Quit();
	}
}
