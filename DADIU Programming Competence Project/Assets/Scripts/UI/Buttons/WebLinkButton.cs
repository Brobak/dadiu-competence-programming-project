﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///	Opens a url when a button is clicked
/// </summary>
public class WebLinkButton : MonoBehaviour
{
	/// <summary>
	/// Opens the given URL
	/// </summary>
	public void OpenURL(string url)
	{
		Application.OpenURL(url);
	}
}
