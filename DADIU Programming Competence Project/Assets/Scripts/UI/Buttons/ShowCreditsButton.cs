﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles whether the menu or credits should be shown or hidden when buttons are clicked
/// </summary>
public class ShowCreditsButton : MonoBehaviour
{
	[SerializeField, NotNull, Tooltip("The canvas under which all the menu items are")]
	private Canvas menuCanvas;
	[SerializeField, NotNull(IgnorePrefab = true), Tooltip("The canvas that holds all the credits")]
	private Canvas creditsCanvas;

	/// <summary>
	/// Shows menu and hides credits or vice versa
	/// </summary>
	public void ShowMenu(bool showMenu)
	{
		menuCanvas.gameObject.SetActive(showMenu);
		creditsCanvas.gameObject.SetActive(!showMenu);
	}
}
