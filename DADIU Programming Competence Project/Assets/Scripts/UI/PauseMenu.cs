﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Handles pausing and unpausing the game as well as any buttons in the menu
/// </summary>
public class PauseMenu : MonoBehaviour
{
    [SerializeField, NotNull, Tooltip("The background and parent object of the menu")]
    private Image menuBackground;

    private bool paused = false;
    private PlayerMovement playerMovement;
    private CameraMovement cameraMovement;
    private MarkSprayer markSprayer;

    private void Awake()
    {
        playerMovement = FindObjectOfType<PlayerMovement>();
        cameraMovement = FindObjectOfType<CameraMovement>();
        markSprayer = FindObjectOfType<MarkSprayer>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;

            SetActiveState();
        }
    }

    /// <summary>
    /// Is called when the resume button is clicked. Resumes the game.
    /// </summary>
    public void ResumeButtonClick()
    {
        paused = false;

        SetActiveState();
    }

    /// <summary>
    /// Is called when the new maze button is clicked. 
    /// Reloads the scene, effectively restarting the game and generating a new maze
    /// </summary>
    public void NewMazeButtonClick()
    {
        paused = false;
        SetActiveState();

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

	/// <summary>
	/// Is called when the main menu button is clicked.
	/// Goes back to the start scene
	/// </summary>
	public void MainMenuButtonClick()
	{
		Time.timeScale = 1;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;

		SceneManager.LoadScene("StartScene");
	}

    private void SetActiveState()
    {
        menuBackground.gameObject.SetActive(paused);

        Time.timeScale = paused ? 0 : 1;

        playerMovement.enabled = !paused;
        cameraMovement.enabled = !paused;
        markSprayer.enabled = !paused;

        Cursor.lockState = paused ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = paused;
    }
}
