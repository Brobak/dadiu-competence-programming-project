﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The base hint to pick up in the maze
/// </summary>
public class PickupHint : MonoBehaviour
{
	protected virtual void OnTriggerEnter(Collider trigger)
	{
		EventBus.TriggerEvent(EventBus.EventType.PickupHint);

		Destroy(gameObject);
	}
}
