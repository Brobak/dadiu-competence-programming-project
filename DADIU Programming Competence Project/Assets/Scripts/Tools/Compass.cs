﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotates the UI compass according to the players rotation in the Y-axis
/// </summary>
public class Compass : MonoBehaviour
{
	[SerializeField, Tooltip("The transform to rotate the compass according to")]
	private Transform target;

	private void Update()
	{
		transform.rotation = Quaternion.Euler(0, 0, target.eulerAngles.y);
	}
}
