﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles what happens when a hint is picked up
/// </summary>
public class HintContext : MonoBehaviour
{
	[SerializeField, NotNull(IgnorePrefab = true), Tooltip("The UI arrow pointing to the goal")]
	private GoalPointer goalPointer;

	[HideInInspector]
	public int hintsPickedup = 0;

	private void Awake()
	{
		EventBus.AddListener(EventBus.EventType.PickupHint, HintPickedUp);
	}

	private void OnDestroy()
	{
		EventBus.RemoveListener(EventBus.EventType.PickupHint, HintPickedUp);
	}

	private void HintPickedUp()
	{
		hintsPickedup++;
		switch(hintsPickedup)
		{
			case 1:
				ActivateGoalPointerArrow();
				break;
			default:
				Debug.Log("Picked up hint number: " + hintsPickedup);
				break;
		}
	}

	private void ActivateGoalPointerArrow()
	{
		goalPointer.gameObject.SetActive(true);
	}
}
