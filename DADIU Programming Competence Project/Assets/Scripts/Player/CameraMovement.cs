﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotates the player and camera according to mouse movement
/// </summary>
public class CameraMovement : MonoBehaviour
{
	[SerializeField, Tooltip("How fast should the camera rotate around the y-axis")]
	private float yRotationSpeed = 5;
	[SerializeField, Tooltip("How fast should the camera rotate around the x-axis")]
	private float xRotationSpeed = 5;

	private Camera cam;
	private float pitch = 0;
	private float yaw = 0;

	private void Awake()
	{
		cam = GetComponentInChildren<Camera>();
	}

	private void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	private void Update()
	{
		float deltaPitch = -xRotationSpeed * Input.GetAxis("Mouse Y");
		if(!((pitch > 90 && deltaPitch > 0) || (pitch < -90 && deltaPitch < 0)))
		{
			pitch += deltaPitch;
		}
		yaw += yRotationSpeed * Input.GetAxis("Mouse X");
		
		SetRotationEuler(pitch, yaw, 0);
	}

	/// <summary>
	/// Rotates the player around the y axis and the camera around x and z
	/// </summary>
	public void SetRotationEuler(float x, float y, float z)
	{
		SetRotationEuler(new Vector3(x, y, z));
	}

	/// <summary>
	/// Rotates the player around the y axis and the camera around x and z
	/// </summary>
	public void SetRotationEuler(Vector3 newRotation)
	{
		transform.localRotation = Quaternion.Euler(0, newRotation.y, 0);
		cam.transform.localRotation = Quaternion.Euler(newRotation.x, 0, newRotation.z);
	}
}
