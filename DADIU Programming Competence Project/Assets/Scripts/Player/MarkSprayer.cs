﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles spraying an image on the floor or the wall in front of the player
/// </summary>
public class MarkSprayer : MonoBehaviour
{
	[SerializeField, Tooltip("The images to spray onto the wall")]
	private SpriteAndTextureTuple[] sprayTextures;
	[SerializeField, NotNull, Tooltip("The prefab to place on the wall when spraying")]
	private Renderer sprayPrefab;
	[SerializeField, Tooltip("The button to activate spray")]
	private KeyCode sprayButton;
	[SerializeField, Tooltip("How far away from the wall can the player be when spraying?")]
	private float maxSprayDistance = 2;
	[SerializeField, Tooltip("The layers that a mark can be sprayed onto")]
	private LayerMask sprayableLayerMask;
	[SerializeField, NotNull(IgnorePrefab = true), Tooltip("The transform to place all spray marks under")]
	private Transform sprayHierarchyParent;
	[SerializeField, NotNull(IgnorePrefab = true), Tooltip("The UI image that shows which mark is painted")]
	private Image sprayIndicator;

	private int chosenSprayTexture = 0;
	private Transform cam;
	private PlayerMovement player;

	private void Awake()
	{
		cam = Camera.main.transform;
		player = FindObjectOfType<PlayerMovement>();
	}

	private void Update()
	{
		if (Input.GetKeyDown(sprayButton))
		{
			SprayMark();
		}
		if (Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			ChangeSprayMark((int)Mathf.Sign(Input.GetAxis("Mouse ScrollWheel")));
		}
	}

	private void SprayMark()
	{
		Ray forward = new Ray(cam.position, cam.forward);
		RaycastHit hit;
		if (Physics.Raycast(forward, out hit, maxSprayDistance, sprayableLayerMask))
		{
			Vector3 sprayPosition = hit.point + hit.normal * 1e-2f;

			if (ClampPosition(ref sprayPosition, hit.normal, sprayPrefab.transform.localScale.x / 2,
				sprayPrefab.transform.localScale.z / 2, hit.collider.bounds.max, hit.collider.bounds.min))
			{
				Renderer newSpray = Instantiate(sprayPrefab, sprayPosition, Quaternion.identity, sprayHierarchyParent);

				newSpray.transform.forward = -hit.normal;
				if (Math.Abs(Vector3.Dot(hit.normal, Vector3.up)) > 1e-10f)
				{
					if (hit.normal.y > 0)
					{
						newSpray.transform.Rotate(hit.normal, player.transform.eulerAngles.y, Space.World);
					}
					else
					{
						newSpray.transform.Rotate(hit.normal, -player.transform.eulerAngles.y + 180, Space.World);
					}
				}

				newSpray.material.mainTexture = sprayTextures[chosenSprayTexture].texture;
			}
		}
	}

	private bool ClampPosition(ref Vector3 position, Vector3 ignoreNormal, float halfWidth,
		float halfHeight, Vector3 maxBounds, Vector3 minBounds)
	{
		if (Math.Abs(Vector3.Dot(ignoreNormal, Vector3.up)) > 1e-10f)
		{
			// normal is up
			return Clamp2DPosition(ref position.x, ref position.z, halfWidth, halfHeight,
				maxBounds.x, minBounds.x, maxBounds.z, minBounds.z);
		}

		if (Math.Abs(Vector3.Dot(ignoreNormal, Vector3.right)) > 1e-10f)
		{
			//normal is right
			return Clamp2DPosition(ref position.z, ref position.y, halfWidth, halfHeight,
				maxBounds.z, minBounds.z, maxBounds.y, minBounds.y);
		}

		if (Math.Abs(Vector3.Dot(ignoreNormal, Vector3.forward)) > 1e-10f)
		{
			//normal is forward
			return Clamp2DPosition(ref position.x, ref position.y, halfWidth, halfHeight,
				maxBounds.x, minBounds.x, maxBounds.y, minBounds.y);
		}

		throw new ArgumentException("The given normal was expected to pe perpendicular");
	}

	private static bool Clamp2DPosition(ref float positionX, ref float positionY, float halfWidth, float halfHeight,
		float maxX, float minX, float maxY, float minY)
	{
		if (maxX - minX < halfWidth * 2)
		{
			return false;
		}
		if (maxY - minY < halfHeight * 2)
		{
			return false;
		}

		positionX = Mathf.Clamp(positionX, minX + halfWidth, maxX - halfWidth);
		positionY = Mathf.Clamp(positionY, minY + halfHeight, maxY - halfHeight);

		return true;
	}

	private void ChangeSprayMark(int spraymarkChange)
	{
		chosenSprayTexture += spraymarkChange;
		chosenSprayTexture = chosenSprayTexture.Mod(sprayTextures.Length);

		sprayIndicator.sprite = sprayTextures[chosenSprayTexture].sprite;
	}

	[Serializable]
	private struct SpriteAndTextureTuple
	{
		public Sprite sprite;
		public Texture texture;
	}
}
