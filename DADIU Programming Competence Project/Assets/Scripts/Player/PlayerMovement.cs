﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The class handling all player movement. Moving in the horizontal plane
/// and moving up and down when close to a ladder.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
	[SerializeField, Tooltip("How fast should the player move")]
	private float moveSpeed = 10;

	private Rigidbody myRigidbody;
	private Camera cam;
	private readonly List<Collider> climbingArea = new List<Collider>();

	private void Awake()
	{
		myRigidbody = GetComponent<Rigidbody>();
		cam = GetComponentInChildren<Camera>();
	}

	private void FixedUpdate()
	{
		if (climbingArea.Any() && IsLookingUp(cam.transform))
		{
			MoveAndClimb();
		}
		else
		{
			MoveHorizontally();
		}
	}

	private void OnTriggerEnter(Collider col)
	{
		if (col.CompareTag("Ladder"))
		{
			climbingArea.Add(col);
			myRigidbody.useGravity = false;
		}
	}

	private void OnTriggerExit(Collider col)
	{
		if (col.CompareTag("Ladder"))
		{
			climbingArea.Remove(col);
			if (!climbingArea.Any())
			{
				myRigidbody.useGravity = true;
			}
		}
	}

	private void MoveHorizontally()
	{
		Vector3 forward = Vector3.ProjectOnPlane(transform.forward, Vector3.up).normalized;
		Vector3 strafe = Vector3.ProjectOnPlane(transform.right, Vector3.up).normalized;

		Vector3 movement = forward * Input.GetAxis("Vertical") + strafe * Input.GetAxis("Horizontal");
		movement.Normalize();

		Vector3 up = Vector3.up * myRigidbody.velocity.y;

		myRigidbody.velocity = movement * moveSpeed + up;
	}

	private void MoveAndClimb()
	{
		Vector3 forward = Vector3.ProjectOnPlane(cam.transform.forward, Vector3.up);
		Vector3 strafe = Vector3.ProjectOnPlane(transform.right, Vector3.up).normalized;
		Vector3 up = Vector3.Project(cam.transform.forward, Vector3.up);
		
		Vector3 movement = (up + forward).normalized * Input.GetAxis("Vertical") + strafe * Input.GetAxis("Horizontal");

		myRigidbody.velocity = movement * moveSpeed;
	}

	private bool IsLookingUp(Transform trans)
	{
		return Vector3.Project(trans.forward, Vector3.up).sqrMagnitude > Vector3.ProjectOnPlane(trans.forward, Vector3.up).sqrMagnitude;
	}
}
