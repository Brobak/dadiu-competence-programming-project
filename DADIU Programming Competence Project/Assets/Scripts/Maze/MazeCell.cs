﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The cells that make up the labyrinth. Mainly keeps track of connections.
/// </summary>
[SelectionBase]
public class MazeCell : CompositeMazeCell
{
	[SerializeField]
	private MazeCell upNeighbour, downNeighbour, leftNeighbour,
		rightNeighbour, forwardNeighbour, backNeighbour;

	[HideInInspector]
	public List<MazeCell> adjacentCells = new List<MazeCell>();
	private int _adjacentUninitializedCells = 6;

	public virtual bool isInitialized { get; private set; }
	public bool isConstructed { get; set; }

	public int adjacentUninitializedCells
	{
		get { return _adjacentUninitializedCells; }
		private set { _adjacentUninitializedCells = value; }
	}

	/// <summary>
	/// Makes this cell and the given cell neighbours
	/// Assumes that this is only called once on each cell as the cell is added to a path. 
	/// Although a cell can be the parameter multiple times. 
	/// In other words a cell can connect once, but be connected to multiple times
	/// </summary>
	public void ConnectTo(MazeCell newNeighbour)
	{
		Vector3 direction = newNeighbour.transform.position - transform.position;
		direction.Normalize();
		MakeNeighbourOf(newNeighbour, direction);
		newNeighbour.MakeNeighbourOf(this, -direction);

		if (!isInitialized)
		{
			isInitialized = true;
			foreach (MazeCell adjacentCell in adjacentCells)
			{
				adjacentCell.adjacentUninitializedCells--;
			}
		}
	}

	/// <summary>
	/// Returns the cell immediately adjacent to this one in the given direction
	/// </summary>
	public MazeCell GetNeighbour(Vector3 direction)
	{
		if (direction == Vector3.forward)
		{
			return forwardNeighbour;
		}
		if (direction == Vector3.back)
		{
			return backNeighbour;
		}
		if (direction == Vector3.left)
		{
			return leftNeighbour;
		}
		if (direction == Vector3.right)
		{
			return rightNeighbour;
		}
		if (direction == Vector3.up)
		{
			return upNeighbour;
		}
		if (direction == Vector3.down)
		{
			return downNeighbour;
		}

		throw new ArgumentException("Expected direction to be up, down, left, right, forward or backward");
	}

	/// <summary>
	/// Setter for the neighbour fields
	/// </summary>
	/// <param name="neighbour">The new neighbour</param>
	/// <param name="direction">The direction to set neighbour as</param>
	public void MakeNeighbourOf(MazeCell neighbour, Vector3 direction)
	{
		if (direction == Vector3.forward)
		{
			forwardNeighbour = neighbour;
		}
		else if (direction == Vector3.back)
		{
			backNeighbour = neighbour;
		}
		else if (direction == Vector3.left)
		{
			leftNeighbour = neighbour;
		}
		else if (direction == Vector3.right)
		{
			rightNeighbour = neighbour;
		}
		else if (direction == Vector3.up)
		{
			upNeighbour = neighbour;
		}
		else if (direction == Vector3.down)
		{
			downNeighbour = neighbour;
		}
		else
		{
			throw new ArgumentException("Direction: " + direction + ".\n" +
										"Expected direction to be up, down, left, right, forward or backward");
		}
	}

	/// <summary>
	/// Adds given cell to this cells adjacent cells and vice versa
	/// </summary>
	public void AddAdjacentCell(MazeCell adjacentCell)
	{
		if (!adjacentCells.Contains(adjacentCell))
		{
			adjacentCells.Add(adjacentCell);
			adjacentCell.adjacentCells.Add(this);

			if (isInitialized)
			{
				adjacentCell.adjacentUninitializedCells--;
			}
			if (adjacentCell.isInitialized)
			{
				adjacentUninitializedCells--;
			}
		}
	}

	/// <summary>
	/// Gets the next cell in the given direction regardless of whether they are connected or not
	/// </summary>
	public MazeCell GetAdjacentCell(Vector3 direction)
	{
		return adjacentCells.FirstOrDefault(
			cell => (cell.transform.position - transform.position).normalized == direction);
	}
}
