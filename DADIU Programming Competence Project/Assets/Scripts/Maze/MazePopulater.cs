﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Puts all the things that should be in the maze in it
/// </summary>
public class MazePopulater
{
	private readonly int goalDistance;
	private readonly int initialMazeSize;
	private readonly float cellsToHintRatio;
	private readonly Goal goalPrefab;
	private readonly PickupHint hintPrefab;
	private readonly Maze maze;

	private Vector3 goalPosition;

	public MazePopulater(int goalDistance, int initialMazeSize, float cellsToHintRatio, Goal goalPrefab, PickupHint hintPrefab, Maze maze)
	{
		this.goalDistance = goalDistance;
		this.initialMazeSize = initialMazeSize;
		this.cellsToHintRatio = cellsToHintRatio;
		this.goalPrefab = goalPrefab;
		this.hintPrefab = hintPrefab;
		this.maze = maze;
	}

	/// <summary>
	/// Creates things in the maze and puts them in their place
	/// Creates a goal and a number of hints
	/// </summary>
	public void PopulateMaze()
	{
		CreateGoal();
		CreateHints();
	}

	private void CreateGoal()
	{
		if (goalDistance > initialMazeSize)
		{
			Debug.LogError("goal is outside of initial maze");
		}

		goalPosition = Vector3.zero;
		Vector3 xAxisStep = Random.Range(0, 2) > 0 ? Vector3.right : Vector3.left,
				yAxisStep = Random.Range(0, 2) > 0 ? Vector3.up : Vector3.down,
				zAxisStep = Random.Range(0, 2) > 0 ? Vector3.forward : Vector3.back;

		for (int i = 0; i < goalDistance; i++)
		{
			switch (Random.Range(0, 3))
			{
				case 0:
					goalPosition += xAxisStep;
					break;
				case 1:
					goalPosition += yAxisStep;
					break;
				case 2:
					goalPosition += zAxisStep;
					break;
			}
		}
		goalPosition = Maze.ScaleToMazeSize(goalPosition);

		GameObject.Instantiate(goalPrefab, goalPosition, Quaternion.identity);
	}

	private void CreateHints()
	{
		int hintsToCreate = Mathf.FloorToInt(maze.Count() / cellsToHintRatio);
		IEnumerable<MazeCell> potentialHintPositions = maze.Where(c =>
			c.transform.position != goalPosition &&
			c.transform.position != Vector3.zero);

		foreach(MazeCell cell in potentialHintPositions.GetRandomElements(hintsToCreate))
		{
			Vector3 hintPosition = cell.transform.position + 
				Maze.ScaleToMazeSize(Vector3.down * 0.45f) + 
				Vector3.up * hintPrefab.transform.localScale.y / 2;

			GameObject.Instantiate(hintPrefab, hintPosition, Quaternion.identity, cell.transform);
		}
	}
}
