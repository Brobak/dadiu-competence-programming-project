﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Holds information about the maze. Is actually a collection of cells that make up the maze
/// </summary>
public class Maze : MonoBehaviour, IEnumerable<MazeCell>
{
	public static float cellWidth = 5, cellLength = 5, cellHeight = 4;
	private const float floatingPrecision = 1e-10f;

	private readonly Dictionary<Vector3, MazeCell> cells = new Dictionary<Vector3, MazeCell>();

	public static Vector3 mazeScale
	{
		get
		{
			return new Vector3(cellWidth, cellLength, cellLength);
		}
	}

	/// <summary>
	/// Gets the maze cell in the given location
	/// </summary>
	public MazeCell this[Vector3 index]
	{
		get { return cells[index]; }
		private set { cells[index] = value; }
	}

	/// <summary>
	/// Scales the given vector to the scale the maze uses (cell width, cell height, cell length)
	/// </summary>
	public static Vector3 ScaleToMazeSize(Vector3 originalSize)
	{
		return new Vector3(originalSize.x * cellWidth, originalSize.y * cellHeight, originalSize.z * cellLength);
	}

	/// <summary>
	/// Scales the given vector down from the scale the maze uses (cell width, cell height, cell length)
	/// </summary>
	public static Vector3 ScaleFromMazeSize(Vector3 originalSize)
	{
		return new Vector3(originalSize.x / cellWidth, originalSize.y / cellHeight, originalSize.z / cellLength);
	}

	/// <summary>
	/// Adds the given maze cell to the maze
	/// </summary>
	public void AddMazeCell(MazeCell cell)
	{
		this[cell.transform.position] = cell;
	}

	/// <summary>
	/// If a cell exists at the given position return true and output the cell
	/// </summary>
	public bool TryGetCell(Vector3 position, out MazeCell output)
	{
		return cells.TryGetValue(position, out output);
	}

	/// <summary>
	/// Get an enumerator of all the maze cells
	/// </summary>
	public IEnumerator<MazeCell> GetEnumerator()
	{
		return cells.Values.GetEnumerator();
	}

	public int Count()
	{
		return cells.Count;
	}

	/// <summary>
	/// Gets the six cells around the one given if they exist
	/// </summary>
	public IEnumerable<MazeCell> GetAdjacentCells(MazeCell cell)
	{
		return GetAdjacentCells(cell.transform.position);
	}

	/// <summary>
	/// Gets the cell at position, as well as all adjacent cells if they exist
	/// </summary>
	/// <param name="center">The center of the star, and the position of the center cell</param>
	/// <returns>A collection of cells</returns>
	public IEnumerable<MazeCell> GetStarFormation(Vector3 center)
	{
		List<MazeCell> starCells = new List<MazeCell>(GetAdjacentCells(center));

		MazeCell tempCell;
		if (TryGetCell(center, out tempCell))
		{
			starCells.Add(tempCell);
		}

		return starCells;
	}

	/// <summary>
	/// Gets the six cells around the given position
	/// </summary>
	public IEnumerable<MazeCell> GetAdjacentCells(Vector3 center)
	{
		if (Math.Abs(center.x % cellWidth) > floatingPrecision ||
			Math.Abs(center.y % cellHeight) > floatingPrecision ||
			Math.Abs(center.z % cellLength) > floatingPrecision)
		{
			throw new ArgumentException("Center must be snapped to maze grid, but was: " + center, "center");
		}

		List<MazeCell> starCells = new List<MazeCell>();

		foreach(Vector3 orthogonalDirection in UtilityConstants.orthogonalDirections.Select(ScaleToMazeSize))
		{
			MazeCell tempCell;
			if(TryGetCell(center + orthogonalDirection, out tempCell))
			{
				starCells.Add(tempCell);
			}
		}

		return starCells;
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}
}
