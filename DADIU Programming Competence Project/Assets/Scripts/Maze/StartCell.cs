﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The start cell (Cell0) counts as initialized from the beginning
/// </summary>
public class StartCell : MazeCell
{
	public override bool isInitialized
	{
		get { return true; }
	}
}
