﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles everything related to creating game objects for maze cells
/// </summary>
[CreateAssetMenu(fileName = "CellConstructor", menuName = "Cell Constructor", order = 100)]
public class CellConstructorVisitor : ScriptableObject
{
    [SerializeField, Tooltip("Dimensions of each cell")]
    private float cellWidth = 5, cellLength = 5, cellHeight = 4;
    [SerializeField, NotNull]
    private MazeCellPart floorPrefab;
    [SerializeField, NotNull]
    private MazeCellPart floorWithHolePrefab;
    [SerializeField, NotNull]
    private MazeCellPart wallPrefab;
	[SerializeField, NotNull]
	private MazeCellPart rotatedWallPrefab;
	[SerializeField, NotNull]
    private MazeCellPart ladderPrefab;

    public void InitializeMaze(Maze maze)
    {
        Maze.cellWidth = cellWidth;
        Maze.cellLength = cellLength;
        Maze.cellHeight = cellHeight;
    }

    /// <summary>
    /// Creates the given cell as a gameobject. Creating walls, floors and whatever the cell is made up of.
    /// </summary>
    /// <param name="cell">The cell to create</param>
    public void VisitCell(MazeCell cell)
    {
        ConstructVertical(cell, Vector3.up);

        ConstructVertical(cell, Vector3.down);

        ConstructHorizontal(cell, Vector3.right, PlaceXWall, PlaceXWall);

        ConstructHorizontal(cell, Vector3.left, PlaceXWall, PlaceXWall);

        ConstructHorizontal(cell, Vector3.forward, PlaceZWall, PlaceZWall);

        ConstructHorizontal(cell, Vector3.back, PlaceZWall, PlaceZWall);

        cell.ScaleTo(cellWidth, cellHeight, cellLength);
        cell.isConstructed = true;
    }

    private void ConstructHorizontal(MazeCell cell, Vector3 direction,
        Action<MazeCell, MazeCell> constructor, Action<MazeCell, Vector3> constructorOuter)
    {
        MazeCell adjacentCell = cell.GetAdjacentCell(direction);
        if (ShouldConstruct(adjacentCell) && !cell.GetNeighbour(direction))
        {
            if (adjacentCell)
            {
                constructor(adjacentCell, cell);
            }
            else
            {
                constructorOuter(cell, direction);
            }
        }
    }

    private void ConstructVertical(MazeCell cell, Vector3 direction)
    {
        MazeCell adjacentCell = cell.GetAdjacentCell(direction);
        if (ShouldConstruct(adjacentCell))
        {
            if (cell.GetNeighbour(direction))
            {
                BuildLadder(adjacentCell, cell);
            }
            else if (adjacentCell)
            {
                PlaceFloor(adjacentCell, cell);
            }
            else
            {
                PlaceFloor(cell, direction);
            }
        }
    }

    private bool ShouldConstruct(MazeCell cell)
    {
        return !cell || !cell.isConstructed;
    }

    private void BuildLadder(MazeCell cell1, MazeCell cell2)
    {
        Vector3 floorPosition = (cell1.transform.position + cell2.transform.position) / 2;
        MazeCellPart newFloor = Instantiate(floorWithHolePrefab, floorPosition, Quaternion.identity, cell1.transform);

        cell1.compositeParts.Add(newFloor);
        cell2.compositeParts.Add(newFloor);

	    MazeCell lowerCell = cell1.transform.position.y < cell2.transform.position.y ? cell1 : cell2;
        MazeCellPart newLadder = Instantiate(ladderPrefab, lowerCell.transform);
        newLadder.transform.localPosition = ladderPrefab.transform.position;

        cell1.compositeParts.Add(newLadder);
        cell2.compositeParts.Add(newLadder);
    }

    private void PlaceFloor(MazeCell lowerCell, MazeCell upperCell)
    {
        Vector3 floorPosition = (lowerCell.transform.position + upperCell.transform.position) / 2;
        MazeCellPart newFloor = Instantiate(floorPrefab, floorPosition, Quaternion.identity, lowerCell.transform);

        lowerCell.compositeParts.Add(newFloor);
        upperCell.compositeParts.Add(newFloor);
    }

    private void PlaceFloor(MazeCell cell, Vector3 direction)
    {
        Vector3 floorPosition = cell.transform.position + direction * cellHeight / 2;
        MazeCellPart newFloor = Instantiate(floorPrefab, floorPosition, Quaternion.identity, cell.transform);

        cell.compositeParts.Add(newFloor);
    }

    private void PlaceZWall(MazeCell forwardCell, MazeCell backwardCell)
    {
        PlaceWall(forwardCell, backwardCell, wallPrefab);
    }

    private void PlaceXWall(MazeCell rightCell, MazeCell leftCell)
    {
        PlaceWall(rightCell, leftCell, rotatedWallPrefab);
    }

    private void PlaceWall(MazeCell cell1, MazeCell cell2, MazeCellPart wall)
    {
        Vector3 wallPosition = (cell1.transform.position + cell2.transform.position) / 2;
        MazeCellPart newWall = Instantiate(wall, wallPosition, Quaternion.identity, cell1.transform);

        cell1.compositeParts.Add(newWall);
        cell2.compositeParts.Add(newWall);
    }

    private void PlaceZWall(MazeCell cell, Vector3 direction)
    {
        Vector3 wallPosition = cell.transform.position + direction * cellLength / 2;
        MazeCellPart newWall = Instantiate(wallPrefab, wallPosition, Quaternion.identity, cell.transform);

        cell.compositeParts.Add(newWall);
    }

    private void PlaceXWall(MazeCell cell, Vector3 direction)
    {
        Vector3 wallPosition = cell.transform.position + direction * cellWidth / 2;
        MazeCellPart newWall = Instantiate(rotatedWallPrefab, wallPosition, Quaternion.identity, cell.transform);

        cell.compositeParts.Add(newWall);
    }
}
