﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Generates a maze for the player to move around in.
/// Uses a modified version of Prim's algorithm by adding new cells to the maze as neighbours of existing cells.
/// </summary>
public class MazeGenerator : MonoBehaviour
{
	[SerializeField, NotNull(IgnorePrefab = true), Tooltip("The cell in which the player starts")]
	private MazeCell startCell;
	[SerializeField, NotNull(IgnorePrefab = true), Tooltip("The cell directly in front of where the player starts")]
	private MazeCell firstCell;
	[SerializeField, Tooltip("How many cells in away from start should the goal be")]
	private int goalDistance = 12;
	[SerializeField, Tooltip("How many cells to create in each direction away from start")]
	private int initialMazeSize = 15;
	[SerializeField, Tooltip("The higher this number is the less hints there are. " +
							"1 means there is a hint in every room.")]
	private float cellsToHintRatio = 8;
	[SerializeField, NotNull, Tooltip("The constructor that creates walls and floors")]
	private CellConstructorVisitor constructor;
	[SerializeField, NotNull, Tooltip("The prefab of an empty maze cell")]
	private MazeCell emptyCellPrefab;
	[SerializeField, NotNull, Tooltip("The prefab of the goal")]
	private Goal goalPrefab;
	[SerializeField, NotNull, Tooltip("The prefab of a hint")]
	private PickupHint hintPrefab;
	[SerializeField, NotNull, Tooltip("The maze object")]
	private Maze maze;

	private int cells = 2;
	private readonly List<MazeCell> outerLayer = new List<MazeCell>();

	protected virtual void Awake()
	{
		constructor.InitializeMaze(maze);
		firstCell.transform.SetPositionZ(Maze.cellLength);
	}

	protected virtual void Start()
	{
		maze.AddMazeCell(startCell);
		maze.AddMazeCell(firstCell);

		CreateInitialCells();

		GenerateBoundedMaze();

		ConstructGeneratedMazeCells();

		MazePopulater populater = new MazePopulater(goalDistance, initialMazeSize, cellsToHintRatio, goalPrefab, hintPrefab, maze);
		populater.PopulateMaze();
	}

	private void CreateInitialCells()
	{
		startCell.AddAdjacentCell(firstCell);
		firstCell.ConnectTo(startCell);
		outerLayer.Add(firstCell);
		CreateAdjacentCells(startCell);

		for (int i = 1; i < initialMazeSize; i++)
		{
			List<MazeCell> tempOuterLayer = new List<MazeCell>(outerLayer);

			foreach (MazeCell cell in tempOuterLayer)
			{
				CreateAdjacentCells(cell);
			}
		}
	}

	private void GenerateBoundedMaze()
	{
		List<MazeCell> nextLayer = startCell.adjacentCells
			.Union(firstCell.adjacentCells)
			.Where(c => !c.isInitialized)
			.ToList();

		while (nextLayer.Any())
		{
			MazeCell nextCell = nextLayer.GetRandomElement();
			nextLayer.Remove(nextCell);

			IEnumerable<MazeCell> connectionCandidates = nextCell.adjacentCells
				.Where(c => c.isInitialized);

			nextCell.ConnectTo(connectionCandidates.GetRandomElement());

			IEnumerable<MazeCell> nextAdditions = nextCell.adjacentCells.Where(c => !c.isInitialized).Except(nextLayer);

			nextLayer.AddRange(nextAdditions);
		}
	}

	protected virtual void ConstructGeneratedMazeCells()
	{
		foreach (MazeCell cell in maze)
		{
			constructor.VisitCell(cell);
		}
	}

	protected void CreateAdjacentCells(MazeCell cell)
	{
		outerLayer.Remove(cell);
		IEnumerable<Vector3> filledDirections = cell.adjacentCells
			.Select(c => (c.transform.position - cell.transform.position).normalized);

		foreach (Vector3 dir in UtilityConstants.orthogonalDirections.Except(filledDirections).Select(Maze.ScaleToMazeSize))
		{
			MazeCell newCell = Instantiate(emptyCellPrefab, cell.transform.position + dir, Quaternion.identity, maze.transform);
			newCell.name = "Cell" + cells++;

			maze.AddMazeCell(newCell);
			IEnumerable<MazeCell> adjacentCells = maze.GetAdjacentCells(newCell);

			foreach (MazeCell adjacentCell in adjacentCells)
			{
				adjacentCell.AddAdjacentCell(newCell);
			}

			if (newCell.adjacentCells.Count < 6)
			{
				outerLayer.Add(newCell);
			}
		}
	}
}
