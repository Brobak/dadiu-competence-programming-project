﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The hole in the floor which aloows players to move up or down
/// </summary>
public class HoleInTheFloor : CompositeMazeCell
{
	public override void ScaleTo(Vector3 scale)
	{
		if (!isScaled)
		{
			base.ScaleTo(scale);

			Transform top = compositeParts[0].transform;
			Transform right = compositeParts[1].transform;
			Transform bottom = compositeParts[2].transform;
			Transform left = compositeParts[3].transform;
			top.localPosition = new Vector3(
				-(scale.x / 2) + (top.localScale.x / 2), 0, 0);
			right.localPosition = new Vector3(
				0, 0, (scale.z / 2) - (right.localScale.z / 2));
			bottom.localPosition = new Vector3(
				(scale.x / 2) - (bottom.localScale.x / 2), 0, 0);
			left.localPosition = new Vector3(
				0, 0, -(scale.z / 2) + (left.localScale.z / 2)); 
		}
	}
}
