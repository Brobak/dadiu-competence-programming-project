﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class represents things in the maze that are made up of multiple parts
/// </summary>
public class CompositeMazeCell : MazeCellPart
{
	public List<MazeCellPart> compositeParts;

	/// <summary>
	/// Scales this object by scaling all it's composite parts
	/// </summary>
	public override void ScaleTo(Vector3 scale)
	{
		if (!isScaled)
		{
			foreach (MazeCellPart compositePart in compositeParts)
			{
				compositePart.ScaleTo(scale);
			}

			isScaled = true;
		}
	}
}
