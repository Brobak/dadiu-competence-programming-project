﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The ladders that allows players to move between floors. Can be scaled to get  more or less steps
/// </summary>
public class Ladder : CompositeMazeCell
{
	[SerializeField, NotNull, Tooltip("A prefab for steps in the ladder")]
	private MazeCellPart stepPrefab;

	public override void ScaleTo(Vector3 scale)
	{
		if (!isScaled)
		{
			MazeCellPart trigger = compositeParts[0];
			MazeCellPart col = compositeParts[1];
			MazeCellPart left = compositeParts[2];
			MazeCellPart right = compositeParts[3];

			trigger.ScaleTo(scale);
			col.ScaleTo(1, scale.y, scale.z);
			left.ScaleTo(1, scale.y, 1);
			right.ScaleTo(1, scale.y, 1);

			float minY = -(scale.y / 2);
			int nrOfSteps = Mathf.Max((int)scale.y, 1);
			float betweenSteps = scale.y / (nrOfSteps + 1);
			MazeCellPart step = null;
			for (int i = 0; i < nrOfSteps; i++)
			{
				step = Instantiate(stepPrefab, transform);
				step.ScaleTo(1, 1, scale.z);

				step.transform.SetLocalPositionY(minY + (i + 1) * betweenSteps);
				compositeParts.Add(step);
			}

			float halfStepWidth = step.transform.localScale.z / 2;
			right.transform.SetLocalPositionZ(-halfStepWidth - right.transform.localScale.z / 2);
			left.transform.SetLocalPositionZ(halfStepWidth + left.transform.localScale.z / 2);

			trigger.transform.SetLocalPositionX(-trigger.transform.localScale.x / 2);

			isScaled = true;
		}
	}
}
