﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An abstract thing that can be in the maze
/// </summary>
public abstract class MazeCellPart : MonoBehaviour
{
	protected bool isScaled = false;

	public void ScaleTo(float x, float y, float z)
	{ 
		if(!isScaled)
		{
			ScaleTo(new Vector3(x, y, z));
		}
	}

	public abstract void ScaleTo(Vector3 scale);
}
