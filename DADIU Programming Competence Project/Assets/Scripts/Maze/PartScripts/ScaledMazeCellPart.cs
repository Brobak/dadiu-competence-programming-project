﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A part of the maze which can be scaled, like floors or walls.
/// </summary>
public class ScaledMazeCellPart : MazeCellPart
{
	[SerializeField]
	private Vector3 scaleRatio;

	public override void ScaleTo(Vector3 scale)
	{
		if (!isScaled)
		{
			if (Mathf.Abs(transform.eulerAngles.y - 90) < 0.001f)
			{
				transform.localScale = new Vector3(
					scale.z * scaleRatio.x,
					scale.y * scaleRatio.y,
					scale.x * scaleRatio.z);
			}
			else
			{
				transform.localScale = new Vector3(
					scale.x * scaleRatio.x,
					scale.y * scaleRatio.y,
					scale.z * scaleRatio.z);
			}

			isScaled = true;
		}
	}
}
