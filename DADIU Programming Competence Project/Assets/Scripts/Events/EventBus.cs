﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Allows any object to subscribe to an event, and fire an event
/// </summary>
public static class EventBus
{
	private static Dictionary<EventType, Action> eventListeners = new Dictionary<EventType,Action>();

	/// <summary>
	/// Adds a listener which will be triggered when the given trigger is fired
	/// </summary>
	public static void AddListener(EventType trigger, Action listener)
	{
		if(eventListeners.ContainsKey(trigger))
		{
			eventListeners[trigger] += listener;
		}
		else
		{
			eventListeners.Add(trigger, listener);
		}
	}

	/// <summary>
	/// Removes a listener which was previously added with <see cref="AddListener"/>
	/// </summary>
	public static void RemoveListener(EventType trigger, Action listener)
	{
		if(eventListeners.ContainsKey(trigger))
		{
			eventListeners[trigger] -= listener;
			if(eventListeners[trigger] == null)
			{
				eventListeners.Remove(trigger);
			}
		}
	}

	/// <summary>
	/// Fires an event of the type specified by trigger
	/// </summary>
	public static void TriggerEvent(EventType trigger)
	{
		if(eventListeners.ContainsKey(trigger))
		{
			eventListeners[trigger]();
		}
	}

	/// <summary>
	/// All the different types of events this system can fire
	/// </summary>
	public enum EventType
	{
		PickupHint,
		WinGame
	}
}
