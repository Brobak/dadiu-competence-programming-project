var indexSectionsWithContent =
{
  0: "acefghilmnoprstuvw",
  1: "cefghlmnpstuw",
  2: "cefghlmnpstuw",
  3: "acegilmnoprstvw",
  4: "achio",
  5: "r",
  6: "e",
  7: "p",
  8: "aimst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties"
};

