var class_vector_closeness_comparer =
[
    [ "VectorClosenessComparer", "class_vector_closeness_comparer.html#a8aaefa6ec907c3e9cf08d3deca5cfb26", null ],
    [ "Equals", "class_vector_closeness_comparer.html#a8469e61319172156ea4f09788a4ae6c2", null ],
    [ "GetHashCode", "class_vector_closeness_comparer.html#a8852b5691534195a491fc7aa9b454487", null ],
    [ "cellHeight", "class_vector_closeness_comparer.html#a5e25ebda763d01165cdaf08b012e833c", null ],
    [ "cellLength", "class_vector_closeness_comparer.html#ad173201bbf09e056cfdb407e49e169fd", null ],
    [ "cellWidth", "class_vector_closeness_comparer.html#ae3386e02432bc681853ac6922af7cf9b", null ],
    [ "floatingPrecision", "class_vector_closeness_comparer.html#abac6085459da158ffcb75906ed823c67", null ]
];